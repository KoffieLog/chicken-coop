#include "SunTime.h"

void SunTime::GetSunrise(const float* lat_lon, const int* date, int* sunrise) {

	CalculateSunTime(lat_lon, date, sunrise, true);
}

void SunTime::GetSunset(const float* lat_lon, const int* date, int* sunset) {

	CalculateSunTime(lat_lon, date, sunset, false);
}

void SunTime::CalculateSunTime(const float* lat_lon, const int* date, int* result, const bool is_sunrise, const float zenith) {

	// Unpack args
	int day = date[2], month = date[1], year = date[0];
	float lat = lat_lon[0], lon = lat_lon[1];

	// (1) Calculate the day of the year
	double N1 = floor(275 * month / 9.0);
	double N2 = floor((month + 9) / 12.0f);
	double N3 = 1 + floor((year - 4 * floor(year / 4.0) + 2.0) / 3.0);
	int N = N1 - (N2 * N3) + day - 30;

	// (2) Convert longitude to houe value and calculate approximat time
	double lon_hour = static_cast<double>(lon) / 15.0;
	double t = is_sunrise ?
		N + ((6.0 - lon_hour) / 24.0) :
		N + ((18.0 - lon_hour) / 24.0);

	// (3) Calculate the Sun's mean anomaly
	double M = (0.9856 * t) - 3.289;

	// (4) Calculate the Sun's true longitude
	double L = M + (1.916f * sin(Deg2Rad * M)) + (0.020 * sin(Deg2Rad * 2 * M)) + 282.634;
	ForceRange<double>(L, 0, 360);

	// (5a) Calculate the Sun's right ascension
	double RA = (1 / Deg2Rad) * atan(0.91764 * tan(Deg2Rad * L));
	ForceRange<double>(RA, 0, 360);

	// (5b) Right ascension value needs to be in the same quadrant as L
	double l_quadrant = floor(L / 90) * 90;
	double r_quadrant = floor(RA / 90) * 90;
	RA += (l_quadrant - r_quadrant);

	// (5c) Right ascension value needs to be converted into hours
	RA /= 15;

	// (6) Calculate the Sun's declination
	double sinDec = 0.39782f * sin(Deg2Rad * L);
	double cosDec = cos(asin(sinDec));

	// (7a) Calculate the Sun's local hour angle
	double cosH = (cos(Deg2Rad * zenith) - (sinDec * sin(Deg2Rad * lat))) / (cosDec * cos(Deg2Rad * lat));
	if (cosH > 1)
		return;     // The sun never rises on this location (on the specified date)
	if (cosH < -1)
		return;     // The sun never sets on this location (on the specified date)


	// (7b) Finish calculating H and convert into hours
	double H = is_sunrise ?
		360 - (1 / Deg2Rad) * acos(cosH) :
		(1 / Deg2Rad) * acos(cosH);
	H /= 15;

	// (8) Calculate local mean time of rise / set
	double T = H + RA - (0.06571 * t) - 6.622;

	// (9) Adjust back to UTC
	double UT = T - lon_hour;
	ForceRange<double>(UT, 0, 24);

	// (10a) Calculate time
	int hour = UT;
	ForceRange<int>(hour, 0, 24);
	int minute = round((UT - floor(UT)) * 60);

	// (10b) Check edge cases
	if (hour == 24) {
		hour = 0;
		day += 1;

		if (day > DaysInMonth(year, month)) {
			day = 1;
			month += 1;

			if (month > 12) {
				month = 1;
				year += 1;
			}
		}
	}

	// Return results
	result[0] = year;
	result[1] = month;
	result[2] = day;
	result[3] = hour;
	result[4] = minute;
}

template<typename T>
void SunTime::ForceRange(T& value, const T min, const T max) {
	if (value < min)
		value += max;
	else if (value >= max)
		value -= max;
}

int SunTime::DaysInMonth(const int year, const int month) {
	return month_days[month] + int(month == February && IsLeapYear(year));
}

bool SunTime::IsLeapYear(const int year) {
	return year % 4 == 0 && (year % 100 != 0 || year % 400 == 0);
}

// Adjusts datetime for timezone and minute offset
void SunTime::AdjustDateTime(int* datetime, const int timezone, const int offset_min) {

	// Apply offset
	datetime[3] += timezone;  	 // timezone
	datetime[4] += offset_min;   // offset minutes

	// Correct hour and minute overflow
	datetime[3] = (24 + datetime[3] + (int)floor(datetime[4] / 60.0)) % 24;
	datetime[4] = (60 + datetime[4]) % 60;
}



