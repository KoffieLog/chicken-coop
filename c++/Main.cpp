///
/// Taylor Frantz 
/// 2022
/// 

#include <iostream>
#include <iomanip>
#include <ctime>

#include "../KippenHoek/SunTime.h"

int main() {
	
	const int local_time_zone = +2;                 // GMT+2
	const int morning_offset = -30;                 // open door 30 min before sunrise
	const int evening_offset = +30;                 // close door 30 min after sunset
	const int begin_summer = 100 * 3 + 26;          // march 26th inclusive
	const int end_summer = 100 * 10 + 29;           // october 30th exclusive

	const float coordinates[2]{ 50.8823, 4.7138 };  // latitude and longitude for Leuven, BE
	
	int sunrise[5]{  };                             // sunrise [year, month, day, hour, min]
	int sunset[5]{  };                              // sunset  [year, month, day, hour, min]

	// Get date
	std::tm gmt{};
	std::time_t t = std::time(0);
	gmtime_s(&gmt, &t);
	int date[3] {
		gmt.tm_year + 1900,
		gmt.tm_mon + 1,
		gmt.tm_mday };

	// Print formatted header
	constexpr int s = 16;
	std::cout << std::left << std::setw(s) << "Date" << std::setw(s) << "Open" << std::setw(s) << "Close" << std::setw(s) << std::endl;

	for (size_t i = 0; i < 5; i++)
	{
		// Calculate sunrise and sunset
		SunTime::GetSunrise(coordinates, date, sunrise);
		SunTime::GetSunset(coordinates, date, sunset);

		// Check for summer / winter date (Mar 26th - Oct 29th)
		int now = 100 * sunrise[1] + sunrise[2];
		int gmt_offset = now >= begin_summer && now < end_summer ?
			local_time_zone :             // is summer time
			local_time_zone - 1;          // is winter time

		// Adjust
		SunTime::AdjustDateTime(sunrise, gmt_offset, morning_offset);
		SunTime::AdjustDateTime(sunset, gmt_offset, evening_offset);

		// Print results
		constexpr char space = ' ';
		std::cout <<
			std::right << 
			std::setfill('0') <<
			std::setw(2) << date[2] << std::setw(1) << '-' <<       // day 
			std::setw(2) << date[1] << std::setw(1) << '-' <<       // month
			std::setw(4) << date[0] <<                              // year
			std::setw(s-10) << std::setfill(space) << space <<      // padding
			std::setfill('0') <<
			std::setw(2) << sunrise[3] << std::setw(1) << ':' <<    // hour 
			std::setw(2) << sunrise[4] <<                           // min
			std::setw(s - 5) << std::setfill(space) << space <<     // padding
			std::setfill('0') <<
			std::setw(2) << sunset[3] << std::setw(1) << ':' <<     // hour 
			std::setw(2) << sunset[4] <<                           // min
			std::endl;

		// Increment data by one day
		date[2]++;
	}
	
	std::cout << std::endl;
	system("pause");
	return 0;
}

