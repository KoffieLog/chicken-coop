import builtins

from utime import sleep_ms, sleep_us
from micropython import const
from machine import Pin, lightsleep, mem32
from button import Button
from stepper import Stepper
from suntime import Sun
import sys, os

from real_time_clock.ds1302 import DS1302 as RTC
from encoder.rotary_rp2 import RotaryIRQ as Encoder

# PIN ASSIGNMENTS
STEPPER_UART_TX = 16  # motor controller uart transmit
STEPPER_UART_RX = 17  # motor controller uart receive
STEPPER_DIR = 18  # motor controller direction
STEPPER_STEP = 19  # motor controller step
STEPPER_ENABLE = 20  # motor controller enable

ENCODER_BTN = 13  # rotary encoder press
ENCODER_CLK = 14  # rotary encoder clk
ENCODER_DT = 15  # rotary encoder dt

RTC_CLK = 9  # real time clock step
RTC_DAT = 8  # real time clock data
RTC_EN = 5  # real time clock enable / reset

# PROGRAM CONSTANTS
NOW_TIME = (2023, 10, 20, 4, 17, 49, 30)  # year, month, day, weekday (Monday = 0), hour, minute, second
MORNING_ALARM = (6, 30)  # default 6:30am
EVENING_ALARM = (22, 15)  # default 10:15pm

MORNING_SHIFT = -30  # number of minutes to adjust the door opening time
EVENING_SHIFT = +30  # number of minutes to adjust the door closing time
BLINK_SPEED = 25  # millisecond
DOOR_WIDTH = 320  # millimeter
DOOR_SPEED = 360  # rev per min
DOOR_SPEED_US = 75  # microsecond
LOCAL_COORDINATES = (50.8823, 4.7138)  # latitude longitude at leuven
LOG_FID = "main.txt"  # name of log file
TIME_ZONE = +2  # local offset from utc

class Controller:

    def __init__(self):

        # Board LED
        self.led = Pin(25, Pin.OUT)

        # Rotary encoder
        self.encoder = Encoder(pin_num_clk=ENCODER_CLK, pin_num_dt=ENCODER_DT, pull_up=True)
        self.encoder.add_listener(self.on_encoder)
        self.encoder_t1 = self.encoder.value()

        # Rotary encoder button
        self.button = Button(pin=ENCODER_BTN)
        self.button.add_listener(self.on_button)

        # Stepper motor
        self.stepper = Stepper(direction=STEPPER_DIR, step=STEPPER_STEP, enable=STEPPER_ENABLE,
                               uart_tx=STEPPER_UART_TX, uart_rx=STEPPER_UART_RX)

        # Real time clock
        self.rtc = RTC(clk=Pin(RTC_CLK), dio=Pin(RTC_DAT), cs=Pin(RTC_EN))
        # self.rtc.date_time(NOW_TIME)

        # Solar calculator
        self.sun = Sun(*LOCAL_COORDINATES)

        # Init default working vars
        self.door_open = False
        self.today = (0, 0, 0)
        self.morning_min = 60 * MORNING_ALARM[0] + MORNING_ALARM[1]
        self.evening_min = 60 * EVENING_ALARM[0] + EVENING_ALARM[1]

        # Try to load any saved working vars
        self.load_state()

        # Enter main loop
        self.main_loop()

    def main_loop(self):

        self.flash(10)  # visual cue
        print("Starting main loop")

        while True:

            # Current time
            second, minute, hour, day, month, _, year = self.rtc.date_time_burst()
            print(f"Main loop update: {day:02}-{month:02}-{year:02} {hour:02}:{minute:02}:{second:02}")

            # Check if today is a new day
            date = (year, month, day)
            if date != self.today:
                self.update_solar_calendar(date)

            # Current minute of the day
            minute += 60 * hour

            # Check if it's time do something
            is_day = self.morning_min <= minute < self.evening_min
            if is_day:
                self.set_day_state()
            else:
                self.set_night_state()

            # Enter low power state
            delta_s = 60 - self.rtc.second()  # seconds to whole min
            delta_m = abs((self.evening_min if is_day else self.morning_min) - minute)  # minutes to next state
            delta_m = min(30, delta_m)  # sleep max 30 min
            sleep_time_ms = (delta_s + delta_m * 60) * 1000  # sleep time in ms
            if Controller.is_connected():
                print(f"Entering low power state for {delta_m}min {delta_s}s")
                sleep_ms(500)
                self.flash(5)  # identify regular sleep
                sleep_ms(sleep_time_ms)  # regular sleep (30mA)
            else:
                lightsleep(sleep_time_ms)  # low power sleep (3mA)

    @staticmethod
    def is_connected():
        ''' returns true if deviec usb bus is connected '''
        SIE_STATUS = const(0x50110000 + 0x50)
        CONNECTED = const(1 << 16)
        SUSPENDED = const(1 << 4)
        return (mem32[SIE_STATUS] & (CONNECTED | SUSPENDED)) == CONNECTED

    def update_solar_calendar(self, now):

        sr = self.sun.get_sunrise_time(now)  # get sunrise
        ss = self.sun.get_sunset_time(now)  # get sunset

        # Shift timing
        sr_min = sr[4] + MORNING_SHIFT
        ss_min = ss[4] + EVENING_SHIFT
        sr_hour = sr[3] + TIME_ZONE
        ss_hour = ss[3] + TIME_ZONE

        # Ensure minutes are in range [0:59]
        if sr_min < 0 or sr_min > 59:
            sr_hour += sr_min // 60
            sr_min %= 60

        if ss_min < 0 or ss_min > 59:
            ss_hour += ss_min // 60
            ss_min %= 60

        # Ensure hours are in range [0:23]
        if sr_hour < 0 or sr_hour > 23:
            sr_hour %= 24

        if ss_hour < 0 or ss_hour > 23:
            ss_hour %= 24

        print(f"The door will open at {sr_hour:02}:{sr_min:02}")
        print(f"The door will close at {ss_hour:02}:{ss_min:02}")

        self.morning_min = sr_hour * 60 + sr_min  # set morning alarm
        self.evening_min = ss_hour * 60 + ss_min  # set evening alarm
        self.today = now

    def set_day_state(self, *args):

        if self.door_open:
            print("The door is already open")
            self.flash(1)
            return

        # Open the door
        print("Opening the door")
        self.stepper.set_forward()
        self.stepper.translate(DOOR_WIDTH, DOOR_SPEED, DOOR_SPEED_US)
        self.door_open = True
        self.save_state()
        self.flash(10)

    def set_night_state(self, *args):

        if not self.door_open:
            print("Door is already closed")
            self.flash(1)
            return

        # Close the door
        print("Closing the door")
        self.stepper.set_reverse()
        self.stepper.translate(DOOR_WIDTH, DOOR_SPEED, DOOR_SPEED_US)
        self.door_open = False
        self.save_state()
        self.flash(10)

    def flash(self, n):
        for i in range(n):
            self.led(1)
            sleep_ms(BLINK_SPEED)
            self.led(0)
            sleep_ms(BLINK_SPEED)

    def on_encoder(self):

        # Feedback
        self.flash(1)

        # Check current encoder value against last value and set direction
        t0 = self.encoder.value()
        is_reverse = self.encoder_t1 > t0
        self.encoder_t1 = t0
        self.stepper.set_reverse() if is_reverse else self.stepper.set_forward()

        # move
        self.stepper.step(10000, DOOR_SPEED_US)

    def on_button(self, event):

        # Button up or down
        button_down = event == Button.DOWN

        if button_down:
            self.led(1)  # flash led
        else:
            # Releasing the button sets the closed door state
            self.door_open = False
            self.save_state()
            self.led(0)

        # TODO: interrupt light_sleep instead in main_loop

    def load_state(self):
        try:
            with open(LOG_FID) as file:
                dict_str = file.read()
                data = eval(dict_str)

            # Load working vars
            print(f"Data loaded from device: {data}")
            self.door_open = data['door_open']

        except OSError:

            # The file does not exist
            self.save_state()

    def save_state(self):
        # Save any working data as a dictionary
        data = {"door_open": self.door_open}

        data_str = str(data)
        with open(LOG_FID, 'w') as file:
            file.write(data_str)
        print(f"Data saved to device: {data_str}")


def main():
    Controller()  # init

def is_connected():
    ''' returns true if deviec usb bus is connected '''
    SIE_STATUS = const(0x50110000 + 0x50)
    CONNECTED = const(1 << 16)
    SUSPENDED = const(1 << 4)
    return (mem32[SIE_STATUS] & (CONNECTED | SUSPENDED)) == CONNECTED




if __name__ == '__main__':


    main()