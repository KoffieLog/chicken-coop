from machine import Pin, UART
from micropython import const
from utime import sleep_us


class Stepper:
    STEPS_PER_ROTATION  = const(200)  # steps per revolution
    MICRO_SEPS          = const(8)    # division per step
    MIN_STEP_TIME       = const(50)   # microseconds
    THREAD_PITCH        = const(8)    # mm
    ENABLE              = const(0)
    DISABLE             = const(1)

    def __init__(self, direction, step, enable, uart_tx, uart_rx):
        self._enable = Pin(enable, Pin.OUT)
        self._step = Pin(step, Pin.OUT)
        self._direction = Pin(direction, Pin.OUT)
        self._step(0)
        self._enable(Stepper.DISABLE)

        tx = Pin(uart_tx, Pin.OUT)
        rx = Pin(uart_rx, Pin.OUT)
        tx(0)
        rx(0)

        # self._uart = UART(0, 115200, 8, None, 1, tx=Pin(uart_tx), rx=Pin(uart_rx))

    def set_forward(self):
        self._direction(0)

    def set_reverse(self):
        self._direction(1)

    def step(self, n, interval_us):

        # enable driver
        self._enable(Stepper.ENABLE)

        for _ in range(n):
            self._step(1)
            sleep_us(interval_us)
            self._step(0)
            sleep_us(interval_us)

        # disable driver
        self._enable(Stepper.ENABLE)

    def rotate(self, n, rpm, force_step_us=None):
        steps_per_rotation = self.STEPS_PER_ROTATION * self.MICRO_SEPS
        steps = round(n * steps_per_rotation)

        us_per_step = round(60E6 / (rpm * steps_per_rotation))
        dt = force_step_us if force_step_us is not None else max(self.MIN_STEP_TIME, us_per_step)

        self.step(steps, dt)

    def translate(self, mm, rpm, force_step_us=None):
        n_rotations = mm / self.THREAD_PITCH
        self.rotate(n_rotations, rpm, force_step_us)

    def get_step_pin(self):
        return self._step