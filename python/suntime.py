import math

# Constants for months referenced later
January = 1
February = 2

# Number of days per month (except for February in leap years)
mdays = [0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]

class SunTimeException(Exception):

    def __init__(self, message):
        super(SunTimeException, self).__init__(message)


class Sun:
    """
    Approximated calculation of sunrise and sunset datetimes. Adapted from:
    https://stackoverflow.com/questions/19615350/calculate-sunrise-and-sunset-times-for-a-given-gps-coordinate-within-postgresql
    """

    def __init__(self, lat, lon):
        self._lat = lat
        self._lon = lon

    def get_sunrise_time(self, date):
        """
        Get sunrise time for a given date in ETC time.
        :param date: Reference date. Today if not provided.
        :return: Local time zone sunrise datetime
        """
        sr = self._calc_sun_time(date, True)
        if sr is None:
            raise SunTimeException('The sun never rises on this location (on the specified date)')
        else:
            return sr

    def get_sunset_time(self, date):
        """
        Get sunset time for a given date in UTC time.
        :param date: Reference date
        :return: Local time zone sunset datetime
        """
        ss = self._calc_sun_time(date, False)
        if ss is None:
            raise SunTimeException('The sun never sets on this location (on the specified date)')
        else:
            return ss

    def _calc_sun_time(self, date, isRiseTime=True, zenith=90.8):
        """
        Calculate sunrise or sunset date.
        :param date: Reference date
        :param isRiseTime: True if you want to calculate sunrise time.
        :param zenith: Sun reference zenith
        :return: UTC sunset or sunrise datetime
        :raises: SunTimeException when there is no sunrise and sunset on given location and date
        """
        # isRiseTime == False, returns sunsetTime
        day = date[2]
        month = date[1]
        year = date[0]

        TO_RAD = math.pi / 180.0

        # 1. first calculate the day of the year
        N1 = math.floor(275 * month / 9)
        N2 = math.floor((month + 9) / 12)
        N3 = (1 + math.floor((year - 4 * math.floor(year / 4) + 2) / 3))
        N = N1 - (N2 * N3) + day - 30

        # 2. convert the longitude to hour value and calculate an approximate time
        lngHour = self._lon / 15

        if isRiseTime:
            t = N + ((6 - lngHour) / 24)
        else:  # sunset
            t = N + ((18 - lngHour) / 24)

        # 3. calculate the Sun's mean anomaly
        M = (0.9856 * t) - 3.289

        # 4. calculate the Sun's true longitude
        L = M + (1.916 * math.sin(TO_RAD * M)) + (0.020 * math.sin(TO_RAD * 2 * M)) + 282.634
        L = self._force_range(L, 360)  # NOTE: L adjusted into the range [0,360)

        # 5a. calculate the Sun's right ascension

        RA = (1 / TO_RAD) * math.atan(0.91764 * math.tan(TO_RAD * L))
        RA = self._force_range(RA, 360)  # NOTE: RA adjusted into the range [0,360)

        # 5b. right ascension value needs to be in the same quadrant as L
        Lquadrant = (math.floor(L / 90)) * 90
        RAquadrant = (math.floor(RA / 90)) * 90
        RA = RA + (Lquadrant - RAquadrant)

        # 5c. right ascension value needs to be converted into hours
        RA = RA / 15

        # 6. calculate the Sun's declination
        sinDec = 0.39782 * math.sin(TO_RAD * L)
        cosDec = math.cos(math.asin(sinDec))

        # 7a. calculate the Sun's local hour angle
        cosH = (math.cos(TO_RAD * zenith) - (sinDec * math.sin(TO_RAD * self._lat))) / (
                    cosDec * math.cos(TO_RAD * self._lat))

        if cosH > 1:
            return None  # The sun never rises on this location (on the specified date)
        if cosH < -1:
            return None  # The sun never sets on this location (on the specified date)

        # 7b. finish calculating H and convert into hours

        if isRiseTime:
            H = 360 - (1 / TO_RAD) * math.acos(cosH)
        else:  # setting
            H = (1 / TO_RAD) * math.acos(cosH)

        H = H / 15

        # 8. calculate local mean time of rising/setting
        T = H + RA - (0.06571 * t) - 6.622

        # 9. adjust back to UTC
        UT = T - lngHour
        UT = self._force_range(UT, 24)  # UTC time in decimal format (e.g. 23.23)

        # 10. Return
        hour = self._force_range(int(UT), 24)
        minute = round((UT - int(UT)) * 60, 0)
        if minute == 60:
            hour += 1
            minute = 0

        # 10. check corner case https://github.com/SatAgro/suntime/issues/1
        if hour == 24:
            hour = 0
            day += 1

            if day > Sun.days_in_month(year, month):
                day = 1
                month += 1

                if month > 12:
                    month = 1
                    year += 1

        datetime = (year, month, day, hour, int(minute))
        return datetime

    @staticmethod
    def _force_range(v, maximum):
        # force v to be >= 0 and < maximum
        if v < 0:
            return v + maximum
        elif v >= maximum:
            return v - maximum

        return v

    @staticmethod
    def days_in_month(year, month):
        """Returns number of days (28-31) for year, month."""

        return mdays[month] + (month == February and Sun.isleap(year))

    @staticmethod
    def isleap(year):
        """Return True for leap years, False for non-leap years."""
        return year % 4 == 0 and (year % 100 != 0 or year % 400 == 0)


if __name__ == '__main__':
    import datetime

    LOCAL_COORDINATES = (50.8823, 4.7138)  # latitude longitude at leuven
    TIME_ZONE = +2                         # local offset from utc
    MORNING_SHIFT = -30                    # number of minutes to adjust the door opening time
    EVENING_SHIFT = +30                    # number of minutes to adjust the door closing time

    # Solar calculator
    sun = Sun(*LOCAL_COORDINATES)

    # Date
    today = datetime.date.today()

    # Print header
    print(f"{'Date':10}\t{'Open':5}\t{'Close'}")

    for n in range(4):

        # Date n days from today
        t = today + datetime.timedelta(days=n)
        date = [t.year, t.month, t.day]

        sr = sun.get_sunrise_time(date)  # get sunrise
        ss = sun.get_sunset_time(date)   # get sunset

        # Shift timing
        sr_min = sr[4] + MORNING_SHIFT
        ss_min = ss[4] + EVENING_SHIFT
        sr_hour = sr[3] + TIME_ZONE
        ss_hour = ss[3] + TIME_ZONE

        # Ensure minutes are in range [0:59]
        if sr_min < 0 or sr_min > 59:
            sr_hour += sr_min // 60
            sr_min %= 60

        if ss_min < 0 or ss_min > 59:
            ss_hour += ss_min // 60
            ss_min %= 60

        # Ensure hours are in range [0:23]
        if sr_hour < 0 or sr_hour > 23:
            sr_hour %= 24

        if ss_hour < 0 or ss_hour > 23:
            ss_hour %= 24

        print(f"{date[2]:02}-{date[1]:02}-{date[0]:04}\t{sr_hour:02}:{sr_min:02}\t{ss_hour:02}:{ss_min:02}")

    input("\nPress enter to exit...")