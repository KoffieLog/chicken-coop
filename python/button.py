from machine import Pin
from micropython import const, schedule

def _trigger(args):
    button_instance, event = args
    for listener in button_instance._listeners:
        listener(event)


class Button:

    DOWN = const(0)
    UP = const(1)

    def __init__(self, pin):
        self.btn = Pin(pin, Pin.IN, Pin.PULL_UP)
        self.btn.irq(handler=self.on_press, trigger=Pin.IRQ_RISING | Pin.IRQ_FALLING)
        self._state = 1
        self._listeners = []

    def add_listener(self, listener):
        self._listeners.append(listener)

    def on_press(self, *args):

        new_state = self.btn.value()
        if self._state == new_state:
            return
        self._state = new_state

        event = Button.UP if new_state else Button.DOWN
        schedule(_trigger, (self, event))

